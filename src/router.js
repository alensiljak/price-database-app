import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './Home'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Home },
    { path: '/settings', component: () => import('./Settings.vue')}
  ]

  const router = new VueRouter({
    mode: 'history',
    routes: routes,
    hashbang: false
  });
  
  export default router;
# price-database-app

The GUI for Price Database project.

## Background

The app is implemented as a Progressive Web Application (PWA) and hosted publicly. The data is synchronized with a private host through a tunnel.
The app can be used on a mobile device and should be synchronized to the central repository.

This is another step in the proof-of-concept for multi-platform applications utilizing standard technologies (HTML+CSS+JS) for the UI, and Python for data processing and storage (SQLite databases).

## Technologies

The UI is built using Vue.js framework, with Bulma CSS, and compiled using Parcel.

## Purpose

The app is the UI for the Price Database. It provides:

- previewing the latest downloaded prices,
- downloading the prices online,
- pruning the prices (deleting the old prices, leaving only the last ones)
